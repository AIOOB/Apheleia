package aioob.apheleia;

import aioob.apheleia.components.Component1;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

public class EntityTest {

    private World world;
    private ComponentMapper<Component1> mapper;

    @Before
    public void before() {
        this.world = new WorldBuilder().requireComponent(Component1.class).build();
        this.mapper = this.world.getMapper(Component1.class);
    }

    @Test
    public void createEntity() {
        HashSet<Integer> entities = new HashSet<>(1000);
        for (int i = 0; i < 1000; i++) {
            int entity = this.world.create();
            assertFalse(entities.contains(entity));
            entities.add(entity);
        }
    }

    @Test
    public void addComponent() {
        int entity = this.world.create();
        Component1 component = new Component1();
        this.mapper.set(entity, component);
        assertEquals(component, this.mapper.get(entity));
        assertTrue(this.mapper.has(entity));
        assertTrue(this.world.hasComponent(entity, Component1.class));
    }

    @Test
    public void removeComponent() {
        int entity = this.world.create();
        Component1 component = new Component1();
        this.mapper.set(entity, component);
        this.mapper.remove(entity);
        assertNull(this.mapper.get(entity));
        assertFalse(this.mapper.has(entity));
        assertFalse(this.world.hasComponent(entity, Component1.class));
    }

    @Test
    public void hasComponent() {
        int entity = this.world.create();
        this.world.getMapper(Component1.class).set(entity, new Component1());
        assertTrue(this.world.hasComponent(entity, Component1.class));
        assertTrue(this.mapper.has(entity));
    }

    @Test
    public void deleteEntity() {
        int entity = this.world.create();
        Component1 component = new Component1();
        this.mapper.set(entity, component);
        this.world.delete(entity);
        assertFalse(this.world.hasComponent(entity, Component1.class));
    }

    @Test
    public void createManyEntities() {
        Component1 component = new Component1();
        int[] entities = new int[1000];
        for (int i = 0; i < entities.length; i++) {
            entities[i] = this.world.create();
            this.mapper.set(entities[i], component);
        }
        for (int entity : entities) {
            assertTrue(this.world.hasComponent(entity, Component1.class));
            assertTrue(this.mapper.has(entity));
        }
    }

    @Test
    public void deleteManyEntities() {
        Component1 component = new Component1();
        int[] entities = new int[1000];
        for (int i = 0; i < entities.length; i++) {
            entities[i] = this.world.create();
            this.mapper.set(entities[i], component);
        }
        for (int entity : entities) {
            this.world.delete(entity);
        }
        for (int entity : entities) {
            assertFalse(this.world.hasComponent(entity, Component1.class));
            assertFalse(this.mapper.has(entity));
        }
    }

    @Test
    public void churnEntities() {
        HashSet<Integer> utilisedIds = new HashSet<>();
        Component1 component = new Component1();
        int[] entities = new int[1000];
        for (int i = 0; i < 10; i++) {
            HashSet<Integer> ids = new HashSet<>();
            for (int j = 0; j < entities.length; j++) {
                entities[j] = this.world.create();
                ids.add(entities[j]);
                this.mapper.set(entities[j], component);
                utilisedIds.add(entities[j]);
            }
            assertEquals(entities.length, ids.size());
            for (int entity : entities) {
                assertTrue(this.world.hasComponent(entity, Component1.class));
                assertTrue(this.mapper.has(entity));
            }
            this.world.process(0.5f);
            for (int entity : entities) {
                this.world.delete(entity);
            }
            for (int entity : entities) {
                assertFalse(this.world.hasComponent(entity, Component1.class));
                assertFalse(this.mapper.has(entity));
            }
        }
        assertEquals(1000, utilisedIds.size());
    }
}
