package aioob.apheleia;

import aioob.apheleia.systems.TagManager;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TagManagerTest {

    private final Object TAG = new Object();
    private TagManager tagManager;
    private World world;
    private int entity;

    @Before
    public void before() {
        this.tagManager = new TagManager();
        this.world = new WorldBuilder().add(this.tagManager).build();
        this.entity = this.world.create();
    }

    @Test
    public void tagEntity() {
        this.tagManager.set(this.TAG, this.entity);
        assertEquals(this.entity, this.tagManager.get(this.TAG));
    }

    @Test
    public void deleteTag() {
        this.tagManager.set(this.TAG, this.entity);
        this.tagManager.remove(this.TAG);
        assertEquals(-1, this.tagManager.get(this.TAG));
    }

    @Test
    public void replaceTaggedEntity() {
        int entity2 = this.world.create();
        this.tagManager.set(this.TAG, this.entity);
        this.tagManager.set(this.TAG, entity2);
        assertEquals(entity2, this.tagManager.get(this.TAG));
    }

    @Test
    public void entityRemoved() {
        this.tagManager.set(this.TAG, this.entity);
        this.world.delete(this.entity);
        assertEquals(-1, this.tagManager.get(this.TAG));
    }

    @Test
    public void multipleTags() {
        Object TAG2 = new Object();
        this.tagManager.set(this.TAG, this.entity);
        this.tagManager.set(TAG2, this.entity);
        assertEquals(this.entity, this.tagManager.get(this.TAG));
        assertEquals(this.entity, this.tagManager.get(TAG2));
    }

    @Test
    public void entityRemovedMultipleTags() {
        Object TAG2 = new Object();
        this.tagManager.set(this.TAG, this.entity);
        this.tagManager.set(TAG2, this.entity);
        this.world.delete(this.entity);
        assertEquals(-1, this.tagManager.get(this.TAG));
        assertEquals(-1, this.tagManager.get(TAG2));
    }
}
