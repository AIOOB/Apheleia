package aioob.apheleia;

import aioob.apheleia.components.Component1;
import aioob.apheleia.components.Component2;
import aioob.apheleia.systems.EntityDeltaSystem;
import aioob.apheleia.systems.EntityTickingSystem;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class EntitySystemTest {

    private final EntityDeltaSystem deltaSystem =
            spy(new EntityDeltaSystem(ComponentMatcher.all(Component1.class)) {
                @Override
                public void process(int entity, float delta, float timeSinceLastTick) {
                }
            });
    private final EntityTickingSystem tickingSystem =
            spy(new EntityTickingSystem(ComponentMatcher.all(Component1.class)) {
                @Override
                public void process(int entity) {
                }
            });
    private final int[] entities = new int[16];
    private World world;
    private ComponentMapper<Component1> mapper;

    @Before
    public void before() {
        reset(this.deltaSystem, this.tickingSystem);
        this.world = new WorldBuilder().add(this.deltaSystem).add(this.tickingSystem)
                .requireComponent(Component1.class).requireComponent(Component2.class).build();
        this.mapper = this.world.getMapper(Component1.class);
        for (int i = 0; i < this.entities.length; i++) {
            this.entities[i] = this.world.create();
            this.mapper.set(this.entities[i], new Component1());
        }
    }

    @Test
    public void order() {
        InOrder deltaOrder = inOrder(this.deltaSystem);
        InOrder tickingOrder = inOrder(this.tickingSystem);
        this.world.process(1.5f);
        deltaOrder.verify(this.deltaSystem).begin(1.5f, 1.5f);
        tickingOrder.verify(this.tickingSystem).begin();
        deltaOrder.verify(this.deltaSystem, atLeastOnce()).process(anyInt(), eq(1.5f), eq(1.5f));
        tickingOrder.verify(this.tickingSystem, times(16)).process(anyInt());
        deltaOrder.verify(this.deltaSystem).end(anyFloat(), anyFloat());
        tickingOrder.verify(this.tickingSystem).end();
    }

    @Test
    public void passingEntities() {
        int nonMatchingEntity = this.world.create();
        this.world.getMapper(Component2.class).set(nonMatchingEntity, new Component2());
        this.world.process(1.5f);
        for (int entity : this.entities) {
            verify(this.deltaSystem).process(entity, 1.5f, 1.5f);
            verify(this.tickingSystem).process(entity);
        }
        verify(this.deltaSystem, never()).process(eq(nonMatchingEntity), anyFloat(), anyFloat());
        verify(this.tickingSystem, never()).process(nonMatchingEntity);
    }

    @Test
    public void removeEntity() {
        this.mapper.remove(this.entities[0]);
        this.world.delete(this.entities[1]);
        this.world.process(1.5f);
        verify(this.deltaSystem, never()).process(eq(this.entities[0]), anyFloat(), anyFloat());
        verify(this.tickingSystem, never()).process(this.entities[1]);
    }

}
