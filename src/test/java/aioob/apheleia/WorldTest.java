package aioob.apheleia;

import aioob.apheleia.components.Component1;
import aioob.apheleia.systems.PublicSystem;
import aioob.apheleia.systems.TestDeltaSystem;
import aioob.apheleia.systems.TestTickingSystem;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class WorldTest {

    @Test
    public void getSystem() {
        PublicSystem system = new PublicSystem();
        World world = new WorldBuilder().add(system).build();
        assertEquals(system, world.get(PublicSystem.class));
    }

    @Test
    public void getDeltaSystem() {
        TestDeltaSystem deltaSystem = new TestDeltaSystem();
        World world = new WorldBuilder().add(deltaSystem).build();
        assertEquals(deltaSystem, world.get(TestDeltaSystem.class));
    }

    @Test
    public void getTickingSystem() {
        TestTickingSystem tickingSystem = new TestTickingSystem();
        World world = new WorldBuilder().add(tickingSystem).build();
        assertEquals(tickingSystem, world.get(TestTickingSystem.class));
    }

    @Test
    public void registerEntityListener() {
        EntityListener listener = mock(EntityListener.class);
        InOrder order = inOrder(listener);
        World world = new WorldBuilder().requireComponent(Component1.class).build();
        world.registerEntityListener(listener);
        ComponentMapper<Component1> mapper = world.getMapper(Component1.class);
        Component1 component = new Component1();
        int entity = world.create();
        mapper.set(entity, component);
        mapper.remove(entity);
        world.delete(entity);
        order.verify(listener).onCreateEntity(entity);
        order.verify(listener).onAddComponent(entity, component);
        order.verify(listener).onRemoveComponent(entity, component);
        order.verify(listener).onDeleteEntity(entity);

    }
}
