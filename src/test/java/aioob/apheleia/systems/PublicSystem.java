package aioob.apheleia.systems;

import aioob.apheleia.ComponentMapper;
import aioob.apheleia.components.Component1;

public class PublicSystem implements System {
    public PublicSystem publicSystem;
    public ComponentMapper<Component1> component1Mapper;
}
