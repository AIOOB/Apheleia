package aioob.apheleia;

import aioob.apheleia.systems.DeltaSystem;
import aioob.apheleia.systems.System;
import aioob.apheleia.systems.TickingSystem;

import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.System.arraycopy;

/**
 * The world as in an ECS architecture that all of the entities reside in.
 */
public final class World {

    /** The number of longs used to store component existence. */
    final int longsPerEntity;
    final ArrayList<EntityListener> entityListeners = new ArrayList<>();
    private final HashMap<Class<? extends System>, System> systemMap;
    private final HashMap<Class<? extends Component>, ComponentMapper<? extends Component>> componentMappers;
    private final DeltaSystem[] deltaSystems;
    private final TickingSystem[] tickingSystems;
    /** An array of bit sets indexed by entity id, used to quickly check if an entity has a component. */
    long[][] componentExistence;
    /** All integers less than or equal to this number not in removedEntities are valid entity ids. */
    private int maxEntity = -1;
    private int[] removedEntities = new int[16];
    private int numRemovedEntities;
    private float timeSinceLastTick;

    /**
     * Constructs a new world with the specified systems and components, called from {@link WorldBuilder} and should not
     * be instantiated any other way.
     *
     * @param componentMappers a map of components to their respective {@link ComponentMapper}
     * @param passiveSystems   a map of all systems including those that implement {@link DeltaSystem} or
     *                         {@link TickingSystem}
     * @param deltaSystems     an array of the systems which implement {@link DeltaSystem}
     * @param tickingSystems   an array of the systems which implement {@link TickingSystem}
     */
    World(HashMap<Class<? extends Component>, ComponentMapper<? extends Component>> componentMappers,
          HashMap<Class<? extends System>, System> passiveSystems,
          DeltaSystem[] deltaSystems, TickingSystem[] tickingSystems) {
        this.componentMappers = componentMappers;
        this.deltaSystems = deltaSystems;
        this.tickingSystems = tickingSystems;
        this.longsPerEntity = (this.componentMappers.size() - 1 >> 6) + 1;
        this.componentExistence = new long[64][this.longsPerEntity];
        this.systemMap = new HashMap<>(this.deltaSystems.length + this.tickingSystems.length);

        for (System system : passiveSystems.values()) {
            this.systemMap.put(system.getClass(), system);
            system.initWorld(this);
            if (system instanceof EntityListener) {
                this.entityListeners.add((EntityListener) system);
            }
        }

        for (DeltaSystem system : this.deltaSystems) {
            this.systemMap.put(system.getClass(), system);
            system.initWorld(this);
            if (system instanceof EntityListener) {
                this.entityListeners.add((EntityListener) system);
            }
        }

        for (TickingSystem system : this.tickingSystems) {
            this.systemMap.put(system.getClass(), system);
            system.initWorld(this);
            if (system instanceof EntityListener) {
                this.entityListeners.add((EntityListener) system);
            }
        }

        for (ComponentMapper mapper : this.componentMappers.values()) {
            mapper.setWorld(this);
        }
    }

    /**
     * Processes all {@link DeltaSystem} and {@link TickingSystem} instances this world was created with.
     * <p>
     * The delta systems are guaranteed to be run first, the ticking systems are then only run if the world time passes
     * an integer. i.e. {@link #timeSinceLastTick} + delta > 1f
     *
     * @param delta the time to advance the world by
     */
    public void process(float delta) {
        this.timeSinceLastTick += delta;

        for (DeltaSystem system : this.deltaSystems) {
            system.process(delta, this.timeSinceLastTick);
        }

        if (this.timeSinceLastTick > 1.0F) {
            this.timeSinceLastTick--;
            for (TickingSystem system : this.tickingSystems) {
                system.process();
            }
        }
    }

    /**
     * Cleanly shuts down this world.
     * <p>
     * <b>DO NOT CALL</b> {@link #process(float) process(delta)} after calling this function, all systems will be
     * shutdown and so the action of process is undefined.
     */
    public void dispose() {
        for (System system : this.systemMap.values()) {
            system.dispose();
        }
    }

    /**
     * Gets the system of the specified type associated with this world. This is slow and so its use should be avoided
     * wherever possible, usually injection into systems is preferred.
     *
     * @param systemClass the class of the system to retrieve
     * @param <T>         the type of the system retrieved
     * @return The requested system or null if it doesn't exist in this world
     */
    @SuppressWarnings("unchecked")
    public <T extends System> T get(Class<T> systemClass) {
        return (T) this.systemMap.get(systemClass);
    }

    /**
     * Gets the mapper for the specified component type, if it exists. This is slow and so its use should be avoided
     * wherever possible, usually injection into systems is preferred.
     *
     * @param componentClass the class of the component type of the returned mapper
     * @param <T> the component type of the returned mapper
     * @return The requested mapper or null if the requested component is unused in this world
     */
    @SuppressWarnings({"WeakerAccess", "unchecked"})
    public <T extends Component> ComponentMapper<T> getMapper(Class<T> componentClass) {
        return (ComponentMapper<T>) this.componentMappers.get(componentClass);
    }

    /**
     * Creates a new entity in this world.
     *
     * @return The newly created entity's id
     */
    public int create() {
        if (this.numRemovedEntities > 0) {
            return this.removedEntities[--this.numRemovedEntities];
        } else {
            this.maxEntity++;
            int newEntity = this.maxEntity;
            if (newEntity == this.componentExistence.length) {
                int newSize = this.componentExistence.length * 2;
                long[][] newArray = new long[newSize][this.longsPerEntity];
                arraycopy(this.componentExistence, 0, newArray, 0, this.componentExistence.length);
                this.componentExistence = newArray;
            }

            for (EntityListener listener : this.entityListeners) {
                listener.onCreateEntity(newEntity);
            }

            return newEntity;
        }
    }

    /**
     * Deletes the specified entity from the world.
     *
     * @param entity the id of the entity to delete
     */
    public void delete(int entity) {
        if (this.numRemovedEntities == this.removedEntities.length) {
            int[] newArray = new int[this.removedEntities.length * 2];
            arraycopy(this.removedEntities, 0, newArray, 0, this.removedEntities.length);
            this.removedEntities = newArray;
        }

        this.removedEntities[this.numRemovedEntities++] = entity;

        for (EntityListener listener : this.entityListeners) {
            listener.onDeleteEntity(entity);
        }

        for (int i = 0; i < this.longsPerEntity; i++) {
            this.componentExistence[entity][i] = 0L;
        }
    }

    /**
     * Manually registers an entity listener, usually unnecessary as all systems are automatically registered.
     *
     * @param listener the listener to register
     */
    public void registerEntityListener(EntityListener listener) {
        this.entityListeners.add(listener);
    }

    /**
     * Gets whether the specified entity has a component of the supplied type.
     *
     * @param entity         the id of the entity to check
     * @param componentClass the class of the component to check for
     * @return {@code true} if the entity has the component, {@code false} otherwise
     */
    public boolean hasComponent(int entity, Class<? extends Component> componentClass) {
        ComponentMapper mapper = this.componentMappers.get(componentClass);
        return mapper != null && this.doesComponentExist(entity, mapper.id);
    }

    /**
     * Gets whether the specified entity has a component with the specified {@link ComponentMapper#id}.
     *
     * @param entity      the entity to check
     * @param componentId the mapper id of the component to check for
     * @return {@code true} if the entity has the component, {@code false} otherwise
     */
    boolean doesComponentExist(int entity, int componentId) {
        int i = componentId >> 6;
        int index = componentId & 63;
        return (this.componentExistence[entity][i] & 1L << index) != 0L;
    }

    /**
     * Sets whether an entity has the specified component.
     *
     * @param entity      the entity to set the component existence for
     * @param componentId the {@link ComponentMapper#id} of the component to set
     * @param exists      whether the component exists on the entity
     */
    void setComponentExists(int entity, int componentId, boolean exists) {
        int i = (componentId >> 6);
        int index = componentId & 63;
        if (exists) {
            this.componentExistence[entity][i] |= 1L << index;
        } else {
            this.componentExistence[entity][i] &= ~(1L << index);
        }
    }
}
