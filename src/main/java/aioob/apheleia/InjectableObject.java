package aioob.apheleia;

import aioob.apheleia.systems.System;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;

/**
 * A wrapper for an object that can be injected into by the injection system.
 * <p>
 * It holds extra metadata about the types that are required by this object, this metadata is initialised on object
 * construction.
 *
 * @param <T> the type of the object that this is wrapping
 */
final class InjectableObject<T extends System> {

    /** A mapping of all the systems that this object requires to be injected. */
    final HashMap<Field, Class<? extends System>> systems = new HashMap<>();
    /**
     * A mapping of all the components that this object utilises, corresponding to
     * {@link ComponentMapper ComponentMappers} that need to be injected
     */
    final HashMap<Field, Class<? extends Component>> components = new HashMap<>();
    final T obj;

    /**
     * Constructs a new {@code InjectableObject} wrapping the specified object.
     * <p>
     * The fields that are required to be injected into are computed here.
     *
     * @param obj the object to wrap
     */
    @SuppressWarnings("unchecked")
    InjectableObject(T obj) {
        this.obj = obj;

        Class<?> clazz = obj.getClass();
        do {
            for (Field field : clazz.getDeclaredFields()) {
                Class type = field.getType();
                if (System.class.isAssignableFrom(type)) {
                    this.systems.put(field, type);
                } else if (ComponentMapper.class.isAssignableFrom(type)) {
                    // Relies heavily on the fact that ComponentMapper is final so types are known
                    ParameterizedType genericType = (ParameterizedType) field.getGenericType();
                    Class<Component> typeArgument = (Class<Component>) genericType.getActualTypeArguments()[0];
                    this.components.put(field, typeArgument);
                }
            }

        } while ((clazz = clazz.getSuperclass()) != Object.class);
        // Could exit slightly earlier but only run once per object during setup
    }
}
