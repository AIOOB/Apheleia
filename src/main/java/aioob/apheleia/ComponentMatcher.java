package aioob.apheleia;

import java.util.ArrayList;
import java.util.Collections;

/**
 * A utility to check if an entity matches a certain set of component existence requirements.
 * all - the entity must have every single one of these components.
 * any - the entity must have at least one of these components.
 * not - the entity must not have any of these components.
 */
public final class ComponentMatcher {

    private final World world;
    /** The bit set of {@link ComponentMapper} ids, that all must be matched. */
    private final long[] all;
    /** The bit set of {@link ComponentMapper} ids, that at least one of must be matched. */
    private final long[] any;
    /** The bit set of {@link ComponentMapper} ids, that none of must be matched. */
    private final long[] not;
    /** The length of the arrays (all have the same length). */
    private final int size;
    /** Used to bypass checking {@link #any}, if all flags are cleared then the standard method would return false. */
    private final boolean emptyAny;

    /**
     * Constructs a new matcher with the specified constraints, it can only be called from {@link Builder}.
     *
     * @param world the world this belongs to
     * @param all   the array of flags that must all match
     * @param any   the array of flags that at least one of must match
     * @param not   the array of flags that none of must match
     */
    private ComponentMatcher(World world, long[] all, long[] any, long[] not) {
        this.world = world;
        this.all = all;
        this.any = any;
        this.not = not;
        this.size = all.length;
        boolean emptyAny = true;
        for (long l : any) {
            emptyAny &= l == 0;
        }
        this.emptyAny = emptyAny;
    }

    /**
     * Constructs a new builder which matches an entity if it has all of the specified components.
     *
     * @param components the components the entity must match
     * @return A builder so that more requirements can be placed on
     */
    @SafeVarargs
    public static Builder all(Class<? extends Component>... components) {
        return new Builder().all(components);
    }

    /**
     * Constructs a new builder which matches an entity if it has any of the specified components.
     *
     * @param components the components the entity must match
     * @return A builder so that more requirements can be placed on
     */
    @SafeVarargs
    public static Builder any(Class<? extends Component>... components) {
        return new Builder().any(components);
    }

    /**
     * Constructs a new builder which matches an entity if it does not have any of the specified components.
     *
     * @param components the components the entity must match
     * @return A builder so that more requirements can be placed on
     */
    @SafeVarargs
    public static Builder not(Class<? extends Component>... components) {
        return new Builder().not(components);
    }

    /**
     * Checks to see if the supplied entity matches the criteria of this matcher.
     *
     * @param entity the entity to check
     * @return {@code true} if the entity matches, {@link false} otherwise
     */
    public boolean matches(int entity) {
        long[] components = this.world.componentExistence[entity];
        boolean matchesAll = true;
        boolean matchesAny = this.emptyAny;
        boolean matchesNot = true;
        for (int i = 0; i < this.size; i++) {
            matchesAll = matchesAll && (this.all[i] & ~components[i]) == 0L;
            matchesAny = matchesAny || (this.any[i] & components[i]) != 0L;
            matchesNot = matchesNot && (this.not[i] & components[i]) == 0L;
        }
        return matchesAll && matchesAny && matchesNot;
    }

    /**
     * A builder for a {@link ComponentMatcher}, allows configuration of all matching criteria before the matcher is
     * constructed.
     */
    public static final class Builder {
        /** A list to temporarily store the component types that must all match. */
        private final ArrayList<Class<? extends Component>> all = new ArrayList<>();
        /** A list to temporarily store the component types that at least one of must match. */
        private final ArrayList<Class<? extends Component>> any = new ArrayList<>();
        /** A list to temporarily store the component types that none of must match. */
        private final ArrayList<Class<? extends Component>> not = new ArrayList<>();

        /**
         * Requires entities matched by this to contain all of the supplied components.
         *
         * @param components the components the entity must match
         * @return This builder to allow chaining.
         */
        @SuppressWarnings("WeakerAccess")
        @SafeVarargs
        public final Builder all(Class<? extends Component>... components) {
            Collections.addAll(this.all, components);
            return this;
        }

        /**
         * Requires entities matched by this to contain any of the supplied components or any preciously provided.
         *
         * @param components the components the entity must match
         * @return This builder to allow chaining.
         */
        @SuppressWarnings("WeakerAccess")
        @SafeVarargs
        public final Builder any(Class<? extends Component>... components) {
            Collections.addAll(this.any, components);
            return this;
        }

        /**
         * Requires entities matched by this to not contain any of the supplied components.
         *
         * @param components the components the entity must match
         * @return This builder to allow chaining.
         */
        @SuppressWarnings("WeakerAccess")
        @SafeVarargs
        public final Builder not(Class<? extends Component>... components) {
            Collections.addAll(this.not, components);
            return this;
        }

        /**
         * Constructs the {@code ComponentMatcher} that will match entities as configured.
         *
         * @param world the world this is in
         * @return The built matcher as configured
         */
        public ComponentMatcher build(World world) {
            long[] allArray = new long[world.longsPerEntity];
            long[] anyArray = new long[world.longsPerEntity];
            long[] notArray = new long[world.longsPerEntity];

            this.fillArray(world, this.all, allArray);
            this.fillArray(world, this.any, anyArray);
            this.fillArray(world, this.not, notArray);
            return new ComponentMatcher(world, allArray, anyArray, notArray);
        }

        /**
         * Sets the bits in the target array, according to the ids of the {@link ComponentMapper ComponentMappers} of
         * the supplied component types.
         *
         * @param world  the world to retrieve the {@link ComponentMapper ComponentMappers} from
         * @param source the components to set bits for
         * @param target the bit field to put the data into
         */
        private void fillArray(World world, ArrayList<Class<? extends Component>> source, long[] target) {
            for (Class<? extends Component> component : source) {
                ComponentMapper mapper = world.getMapper(component);
                if (mapper == null) continue;
                int i = mapper.id >>> 6;
                int index = mapper.id & 63;
                target[i] = target[i] | (1L << index);
            }
        }
    }
}
