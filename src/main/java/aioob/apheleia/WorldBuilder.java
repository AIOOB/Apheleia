package aioob.apheleia;

import aioob.apheleia.systems.DeltaSystem;
import aioob.apheleia.systems.System;
import aioob.apheleia.systems.TickingSystem;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Constructs a {@link World} with the specified systems.
 */
public final class WorldBuilder {

    /**
     * A map of the systems that don't implement {@link DeltaSystem} or {@link TickingSystem}, stored as
     * {@code Class -> System} so that they can be easily requested by the user from {@link World}.
     */
    private final HashMap<Class<? extends System>, System> passiveSystems = new HashMap<>();
    /**
     * A linked set storing the {@link DeltaSystem DeltaSystems}, this is used to preserve the order that the systems
     * are added in.
     */
    private final LinkedHashSet<DeltaSystem> deltaSystems = new LinkedHashSet<>();
    /**
     * A linked set storing the {@link TickingSystem TickingSystems}, this is used to preserve the order that the
     * systems are added in.
     */
    private final LinkedHashSet<TickingSystem> tickingSystems = new LinkedHashSet<>();
    /**
     * A map of all the {@link InjectableObject} wrappers for the systems, stored as {@code Class -> InjectableObject}
     * so that they can easily be retrieved by type.
     */
    private final HashMap<Class, InjectableObject<?>> injectables = new HashMap<>();
    /**
     * A set of all the types of components that are utilised by the systems, and possibly some user added ones.
     */
    private final HashSet<Class<? extends Component>> componentClasses = new HashSet<>();
    /**
     * A map of the component types to component mappers, stored as {@code Class<Type> -> ComponentMapper<Type>} to
     * allow easy lookup and injection into the required classes.
     */
    private final HashMap<Class<? extends Component>, ComponentMapper<? extends Component>> componentMappers = new HashMap<>();

    /**
     * Adds the specified system to the world when it is created, only one of each type of system may be added.
     * <p>
     * Order is important, the {@link TickingSystem} and {@link DeltaSystem} will be run in the same order that they are
     * supplied to this function, but no guarantee is made about the order that the two groups are run in.
     * <p>
     * All the dependencies of the added system should also be added at some point or build will fail.
     *
     * @param system the system to add to the world
     * @return This builder to allow chaining
     */
    @SuppressWarnings("WeakerAccess")
    public WorldBuilder add(System system) {
        boolean added;
        if (system instanceof DeltaSystem) {
            added = this.deltaSystems.add((DeltaSystem) system);
        } else if (system instanceof TickingSystem) {
            added = this.tickingSystems.add((TickingSystem) system);
        } else {
            added = this.passiveSystems.putIfAbsent(system.getClass(), system) == null;
        }
        if (!added) {
            throw new IllegalArgumentException("A system of type " + system.getClass().getSimpleName() +
                    " has already been added.");
        }
        this.injectables.put(system.getClass(), new InjectableObject<>(system));
        return this;
    }

    /**
     * Sets the supplied component as required and so a {@link ComponentMapper} will be created for it.
     * <p>
     * It should not be necessary to ever call this method as any component matcher referenced to by a system will be
     * automatically created. This is only necessary if the component is not used by any system, which is not advised.
     *
     * @param componentClass the class of the component that is required to have a mapper
     * @return This builder to allow chaining
     */
    public WorldBuilder requireComponent(Class<? extends Component> componentClass) {
        this.componentClasses.add(componentClass);
        return this;
    }

    /**
     * Constructs the {@link World} as has been configured, this world builder is left in an undefined state and so
     * should be allowed to be garbage collected after this method is called.
     * <p>
     * All the dependencies of each system should have been added or this will fail.
     *
     * @return The constructed world with the specified systems
     */
    @SuppressWarnings("WeakerAccess")
    public World build() {
        // Adds all of the components referenced to by the systems
        for (InjectableObject<?> injectableObject : this.injectables.values()) {
            this.componentClasses.addAll(injectableObject.components.values());
        }

        // Creates all of the ComponentMappers to be injected into the systems
        for (Class<? extends Component> clazz : this.componentClasses) {
            this.componentMappers.put(clazz, new ComponentMapper<>());
        }

        try {
            for (InjectableObject<?> injectionTarget : this.injectables.values()) {
                // Injects Systems
                for (Map.Entry<Field, Class<? extends System>> entry : injectionTarget.systems.entrySet()) {
                    Field field = entry.getKey();
                    Class clazz = entry.getValue();
                    InjectableObject injectionSource = this.injectables.get(clazz);
                    if (injectionSource == null) {
                        throw new UnavailableInjectionException(clazz);
                    }
                    field.setAccessible(true);
                    // injectionSource is injected into injectionTarget
                    field.set(injectionTarget.obj, injectionSource.obj);
                }

                // Injects ComponentMappers
                for (Map.Entry<Field, Class<? extends Component>> entry : injectionTarget.components.entrySet()) {
                    entry.getKey().setAccessible(true);
                    entry.getKey().set(injectionTarget.obj, this.componentMappers.get(entry.getValue()));
                }
            }
        } catch (IllegalAccessException e) {
            // Code shouldn't ever reach here as setAccessible is used.
            throw new RuntimeException(e);
        }

        DeltaSystem[] deltaSystemArray = this.deltaSystems.toArray(new DeltaSystem[this.deltaSystems.size()]);
        TickingSystem[] tickingSystemArray = this.tickingSystems.toArray(new TickingSystem[this.tickingSystems.size()]);
        return new World(this.componentMappers, this.passiveSystems, deltaSystemArray, tickingSystemArray);
    }
}
