package aioob.apheleia;

/**
 * A listener for changes in the entities in the world: creation, deletion, and component addition and removal.
 */
public interface EntityListener {

    /**
     * Called when an entity is created and added to the world.
     *
     * @param entity the entity that has been created
     */
    default void onCreateEntity(int entity) {
    }

    /**
     * Called when an entity is deleted from the world.
     *
     * @param entity the entity that has been deleted
     */
    default void onDeleteEntity(int entity) {
    }

    /**
     * Called when a component is added to an entity.
     *
     * @param entity    the entity the component was added to
     * @param component the component that was added
     */
    default void onAddComponent(int entity, Component component) {
    }

    /**
     * Called when a component is removed from an entity.
     *
     * @param entity    the entity the component was removed from
     * @param component the component that was removed
     */
    default void onRemoveComponent(int entity, Component component) {
    }
}
