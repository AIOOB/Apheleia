package aioob.apheleia.systems;

/**
 * A system that is called as often as possible, provides a single method {@link #process}.
 */
public interface DeltaSystem extends System {

    /**
     * Processes changes to the world, both parameters are in world time which is as provided to
     * {@link aioob.apheleia.World#process(float) World.process(delta)}. A world tick occurs when the accumulated value
     * of this time passes an integer, so {@code timeSinceLastTick} usually lies in [0,1] unless a value larger than 1
     * is passed into World.process(delta).
     *
     * @param delta             the world time since the last invocation of this method
     * @param timeSinceLastTick the world time elapsed since the last tick
     */
    void process(float delta, float timeSinceLastTick);
}
