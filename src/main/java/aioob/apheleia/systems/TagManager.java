package aioob.apheleia.systems;

import aioob.apheleia.EntityListener;

import java.util.HashMap;

/**
 * A system that can be used to tag entities with objects, it is recommended to use enum values as they are guaranteed
 * to remain constant throughout execution.
 */
public final class TagManager implements System, EntityListener {

    private final HashMap<Object, Integer> taggedEntities = new HashMap<>();

    /**
     * Sets the tag for the specified entity, if an entity had been previously tagged then the old entity is replaced.
     *
     * @param tag    the object to tag this entity with
     * @param entity the entity to tag
     * @return The previous tag or {@code null} if this entity was not previously tagged.
     */
    public Object set(Object tag, int entity) {
        return this.taggedEntities.put(tag, entity);
    }

    /**
     * Removes the tag from the entity currently tagged entity.
     *
     * @param tag the object to remove from the list of tags
     */
    public void remove(Object tag) {
        this.taggedEntities.remove(tag);
    }

    /**
     * Gets the entity that is tagged by the supplied object. Warning, this uses {@link Object#equals(Object)} so there
     * may be unexpected results if using mutable values or strings as tags.
     *
     * @param tag the tag to lookup
     * @return The entity that is tagged by {@code tag}, or -1 if no entity has this tag
     */
    public int get(Object tag) {
        Integer entity = this.taggedEntities.get(tag);
        return entity == null ? -1 : entity;
    }

    @Override
    public void onDeleteEntity(int entity) {
        this.taggedEntities.entrySet().removeIf(entry -> entry.getValue() == entity);
    }
}
