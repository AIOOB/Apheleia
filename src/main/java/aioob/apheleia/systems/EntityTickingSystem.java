package aioob.apheleia.systems;

import aioob.apheleia.ComponentMatcher;

/**
 * An {@code EntitySystem} that follows the contract of {@code TickingSystem} so is called once per tick by the world.
 */
public abstract class EntityTickingSystem extends EntitySystem implements TickingSystem {

    /**
     * Constructs an {@code EntityTickingSystem} which operates on the entities as specified by the provided
     * {@link ComponentMatcher}.
     *
     * @param componentMatcherBuilder a builder configured to match the entities that this should operate on
     */
    public EntityTickingSystem(ComponentMatcher.Builder componentMatcherBuilder) {
        super(componentMatcherBuilder);
    }

    /**
     * Runs before this system starts iterating through its entities, used for setup.
     */
    @SuppressWarnings("WeakerAccess")
    public void begin() {
    }

    /**
     * Runs once per entity that this system operates on with the same values as was passed into {@link #process()}.
     *
     * @param entity the entity it is operating on
     */
    @SuppressWarnings("WeakerAccess")
    public abstract void process(int entity);

    /**
     * Runs after this system stops iterating through its entities
     */
    @SuppressWarnings("WeakerAccess")
    public void end() {
    }

    @Override
    public void process() {
        this.begin();

        for (int entity = 0; entity < this.entities.length(); ++entity) {
            entity = this.entities.nextSetBit(entity);
            this.process(entity);
        }

        this.end();
    }
}
