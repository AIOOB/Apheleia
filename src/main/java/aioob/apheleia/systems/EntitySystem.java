package aioob.apheleia.systems;

import aioob.apheleia.Component;
import aioob.apheleia.ComponentMatcher;
import aioob.apheleia.EntityListener;
import aioob.apheleia.World;

import java.util.BitSet;

/**
 * A system that maintains a list of entities that it operates on, a {@link ComponentMatcher} is used to specify the
 * entities that it stores.
 */
public abstract class EntitySystem implements System, EntityListener {

    /** A set of all the entities that this system operates on. */
    @SuppressWarnings("WeakerAccess")
    protected final BitSet entities;
    /** Temporarily store the builder between construction and {@link #initWorld(World)}. */
    private final ComponentMatcher.Builder componentMatcherBuilder;
    /** The matcher that this uses to check if an entity should be operated on. */
    @SuppressWarnings("WeakerAccess")
    protected ComponentMatcher componentMatcher;

    /**
     * Constructs an {@code EntitySystem} which operates on the entities as specified by the provided
     * {@link ComponentMatcher}.
     *
     * @param componentMatcherBuilder a builder configured to match the entities that this should operate on
     */
    @SuppressWarnings("WeakerAccess")
    public EntitySystem(ComponentMatcher.Builder componentMatcherBuilder) {
        this.componentMatcherBuilder = componentMatcherBuilder;
        this.entities = new BitSet(64);
    }

    /**
     * Initialises the {@link ComponentMatcher} for this system.
     *
     * @param world the world this system is in
     */
    @Override
    public void initWorld(World world) {
        if (this.componentMatcher == null) {
            this.componentMatcher = this.componentMatcherBuilder.build(world);
        }
    }

    @Override
    public final void onAddComponent(int entity, Component component) {
        if (this.componentMatcher.matches(entity)) {
            this.entities.set(entity, true);
        }
    }

    @Override
    public final void onRemoveComponent(int entity, Component component) {
        if (!this.componentMatcher.matches(entity)) {
            this.entities.set(entity, false);
        }
    }

    @Override
    public void onDeleteEntity(int entity) {
        this.entities.clear(entity);
    }
}
