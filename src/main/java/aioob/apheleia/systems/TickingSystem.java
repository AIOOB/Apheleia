package aioob.apheleia.systems;

/**
 * A system that is called once per world tick, which occurs whenever world time passes an integer value.
 */
public interface TickingSystem extends System {

    /**
     * Processes changes to the world since the last tick.
     */
    void process();
}
